package com.ymw.mall.file.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author dongfengfan
 * @date 2021/7/24 下午3:52
 */
@Data
@Configuration
public class OssConfiguration {

    @Value("${endpoint}")
    private String endPoint;

    @Value("${accessKeyId}")
    private String accessKeyId;

    @Value("${accessKeySecret}")
    private String accessKeySecret;

    @Value("${filehost}")
    private String fileHost;

    @Value("${bucketName}")
    private String bucketName;

}

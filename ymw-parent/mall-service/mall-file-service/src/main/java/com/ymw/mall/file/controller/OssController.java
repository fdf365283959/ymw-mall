package com.ymw.mall.file.controller;

import cn.hutool.core.util.StrUtil;
import com.ymw.mall.file.util.AliyunOssUtil;
import com.ymw.mall.util.RespCode;
import com.ymw.mall.util.RespResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author dongfengfan
 * @date 2021/7/24 下午3:52
 */

@RestController
@RequestMapping(value = "/file")
@Api(tags = "文件上传下载接口")
public class OssController {

    @Autowired
    private AliyunOssUtil ossUtil;

    @ApiOperation("上传文件")
    @PostMapping("/upload")
    public RespResult<String> upload(@RequestParam("file") MultipartFile file) {
        String baseUrl = "http://fan333.oss-cn-shanghai.aliyuncs.com/";
        if (file == null) {
            return RespResult.error("文件不能为空");
        }
        String fileName = file.getOriginalFilename();
        if (StrUtil.isBlank(fileName)) {
            return RespResult.error("文件路径不能为空");
        }
        try {
            File newFile = new File(fileName);
            FileOutputStream os = new FileOutputStream(newFile);
            os.write(file.getBytes());
            os.close();
            //把file里的内容复制到奥newFile中
            file.transferTo(newFile);
            String upload = ossUtil.upload(newFile);
            return RespResult.ok(baseUrl + upload);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return RespResult.error(RespCode.SYSTEM_ERROR);
    }

}


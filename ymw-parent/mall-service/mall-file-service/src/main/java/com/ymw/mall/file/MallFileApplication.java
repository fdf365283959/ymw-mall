package com.ymw.mall.file;


import com.ymw.mall.config.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * @author dongfengfan
 * @date 2021/7/24 上午15:34
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@Import(SwaggerConfig.class)
public class MallFileApplication {

    public static void main(String[] args) {
        SpringApplication.run(com.ymw.mall.file.MallFileApplication.class, args);
    }

}

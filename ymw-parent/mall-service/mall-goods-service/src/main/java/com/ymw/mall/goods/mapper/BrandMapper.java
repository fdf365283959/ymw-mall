package com.ymw.mall.goods.mapper;

import com.ymw.mall.goods.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 品牌表 Mapper 接口
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Repository
public interface BrandMapper extends BaseMapper<Brand> {

    /**
     * 根据分类ID查询品牌集合
     * @param id 分类id
     * @return 品牌id集合
     */
    @Select("select brand_id from category_brand where category_id=#{id}")
    List<Integer> queryBrandIds(Integer id);

}

package com.ymw.mall.goods.mapper;

import com.ymw.mall.goods.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 商品类目 Mapper 接口
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Repository
public interface CategoryMapper extends BaseMapper<Category> {

}

package com.ymw.mall.goods.mapper;

import com.ymw.mall.goods.entity.CategoryAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 属性分类表 Mapper 接口
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Repository
public interface CategoryAttrMapper extends BaseMapper<CategoryAttr> {

}

package com.ymw.mall.goods;


import com.ymw.mall.config.SwaggerConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * @author dongfengfan
 * @date 2021/7/24 上午15:34
 */
@SpringBootApplication
@Import(SwaggerConfig.class)
@MapperScan("com.ymw.mall.goods.mapper")
public class MallGoodsApplication {

    public static void main(String[] args) {
        SpringApplication.run(com.ymw.mall.goods.MallGoodsApplication.class, args);
    }

}

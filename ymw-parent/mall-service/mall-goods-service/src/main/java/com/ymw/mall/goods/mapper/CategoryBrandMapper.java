package com.ymw.mall.goods.mapper;

import com.ymw.mall.goods.entity.CategoryBrand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 分类品牌关系表 Mapper 接口
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Repository
public interface CategoryBrandMapper extends BaseMapper<CategoryBrand> {

}

package com.ymw.mall.goods.service.impl;

import com.ymw.mall.goods.entity.CategoryAttr;
import com.ymw.mall.goods.mapper.CategoryAttrMapper;
import com.ymw.mall.goods.service.CategoryAttrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 属性分类表 服务实现类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Service
public class CategoryAttrServiceImpl extends ServiceImpl<CategoryAttrMapper, CategoryAttr> implements CategoryAttrService {

}

package com.ymw.mall.goods.service;

import com.ymw.mall.goods.entity.Brand;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 品牌表 服务类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
public interface BrandService extends IService<Brand> {

    /**
     * 根据分类ID查询品牌集合
     * @param id 分类id
     * @return 品牌id集合
     */
    List<Brand> queryByCategoryId(Integer id);

}

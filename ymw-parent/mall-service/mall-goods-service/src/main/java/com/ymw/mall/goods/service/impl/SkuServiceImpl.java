package com.ymw.mall.goods.service.impl;

import com.ymw.mall.goods.entity.Sku;
import com.ymw.mall.goods.mapper.SkuMapper;
import com.ymw.mall.goods.service.SkuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements SkuService {

}

package com.ymw.mall.goods.code;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @author dongfengfan
 * @date 2021/8/4 上午10:12
 */
public class MybatisPlusGenerator {

    public static void main(String[] args) {
        AutoGenerator mpg = new AutoGenerator();
        //1、全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = "/Users/dongfengfan/IdeaProjects/ymw-mall/ymw-parent/mall-service/mall-goods-service";
        //生成路径(一般都是生成在此项目的src/main/java下面)
        gc.setOutputDir(projectPath + "/src/main/java");
        //设置作者
        gc.setAuthor("dongfengfan");
        gc.setOpen(false);
        //第二次生成会把第一次生成的覆盖掉
        gc.setFileOverride(true);
        //生成的service接口名字首字母是否为I，这样设置就没有
        gc.setServiceName("%sService");
        gc.setSwagger2(true);
        mpg.setGlobalConfig(gc);

        //2、数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/shop_goods?characterEncoding=utf8&serverTimezone=UTC&useSSL=false");
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        mpg.setDataSource(dsc);

        // 3、包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.ymw.mall");
        pc.setModuleName("goods");
        mpg.setPackageInfo(pc);

        // 4、策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // strategy.setTablePrefix("t_"); // 表名前缀
        //使用lombok
        strategy.setEntityLombokModel(true);
        // 逆向工程使用的表   如果要生成多个,这里可以传入String[]
        strategy.setInclude("brand", "category", "category_attr", "category_brand", "sku", "sku_attribute", "spu");
        mpg.setStrategy(strategy);

        //5、执行
        mpg.execute();
    }

}

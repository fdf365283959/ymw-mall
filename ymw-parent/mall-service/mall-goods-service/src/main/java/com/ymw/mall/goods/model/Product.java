package com.ymw.mall.goods.model;

import com.ymw.mall.goods.entity.Sku;
import com.ymw.mall.goods.entity.Spu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author dongfengfan
 * @date 2021/8/4 下午13:52
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="商品对象")
public class Product {

    @ApiModelProperty("spu对象实体")
    private Spu spu;
    @ApiModelProperty("sku对象集合")
    private List<Sku> skus;

}
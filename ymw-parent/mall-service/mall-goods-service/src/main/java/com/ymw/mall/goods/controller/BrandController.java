package com.ymw.mall.goods.controller;


import com.ymw.mall.goods.entity.Brand;
import com.ymw.mall.goods.service.BrandService;
import com.ymw.mall.util.RespResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 品牌表 前端控制器
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@RestController
@RequestMapping("/goods/brand")
@Api(tags = "品牌相关接口")
public class BrandController {

    @Autowired
    BrandService brandService;

    /****
     * 根据分类ID查询品牌
     */
    @GetMapping(value = "/category/{id}")
    @ApiOperation("根据分类ID查询品牌")
    public RespResult<List<Brand>> categoryBrands(@PathVariable(value = "id")Integer id){
        List<Brand> brands = brandService.queryByCategoryId(id);
        return RespResult.ok(brands);
    }

}


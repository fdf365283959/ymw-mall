package com.ymw.mall.goods.service;

import com.ymw.mall.goods.entity.Spu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ymw.mall.goods.model.Product;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
public interface SpuService extends IService<Spu> {

    //保存商品
    void saveProduct(Product product);

}

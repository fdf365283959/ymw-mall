package com.ymw.mall.goods.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ymw.mall.goods.entity.Brand;
import com.ymw.mall.goods.mapper.BrandMapper;
import com.ymw.mall.goods.service.BrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 品牌表 服务实现类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements BrandService {

    @Autowired
    BrandMapper brandMapper;

    /***
     * 根据分类ID查询品牌
     * @param id
     * @return list
     */
    @Override
    public List<Brand> queryByCategoryId(Integer id) {
        //查询分类ID对应的品牌集合
        List<Integer> brandIds = brandMapper.queryBrandIds(id);
        if (CollUtil.isEmpty(brandIds)) {
            return new ArrayList<>();
        }
        //根据品牌ID集合查询品牌信息
        return this.listByIds(brandIds);
    }

}

package com.ymw.mall.goods.service.impl;

import com.ymw.mall.goods.entity.CategoryBrand;
import com.ymw.mall.goods.mapper.CategoryBrandMapper;
import com.ymw.mall.goods.service.CategoryBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分类品牌关系表 服务实现类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Service
public class CategoryBrandServiceImpl extends ServiceImpl<CategoryBrandMapper, CategoryBrand> implements CategoryBrandService {

}

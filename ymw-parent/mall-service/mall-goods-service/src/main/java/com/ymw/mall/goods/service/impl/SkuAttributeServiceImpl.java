package com.ymw.mall.goods.service.impl;

import com.ymw.mall.goods.entity.SkuAttribute;
import com.ymw.mall.goods.mapper.SkuAttributeMapper;
import com.ymw.mall.goods.service.SkuAttributeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 属性表 服务实现类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Service
public class SkuAttributeServiceImpl extends ServiceImpl<SkuAttributeMapper, SkuAttribute> implements SkuAttributeService {

    @Autowired
    SkuAttributeMapper skuAttributeMapper;

    /***
     * 根据分类ID查询属性集合
     * @param id
     * @return
     */
    @Override
    public List<SkuAttribute> queryList(Integer id) {
        return skuAttributeMapper.queryByCategoryId(id);
    }

}

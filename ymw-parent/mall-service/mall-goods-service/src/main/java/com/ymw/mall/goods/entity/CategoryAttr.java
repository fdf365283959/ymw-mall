package com.ymw.mall.goods.entity;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 属性分类表
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="CategoryAttr对象", description="属性分类表")
public class CategoryAttr implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer categoryId;

    @ApiModelProperty(value = "属性分类id")
    private Integer attrId;


}

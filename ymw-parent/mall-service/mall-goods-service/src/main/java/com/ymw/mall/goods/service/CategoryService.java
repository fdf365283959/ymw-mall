package com.ymw.mall.goods.service;

import com.ymw.mall.goods.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品类目 服务类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
public interface CategoryService extends IService<Category> {

    /**
     * 根据父ID查询子分类
     * @param pid
     * @return
     */
    List<Category> queryByParentId(Integer pid);

}

package com.ymw.mall.goods.controller;


import com.ymw.mall.goods.model.Product;
import com.ymw.mall.goods.service.SpuService;
import com.ymw.mall.util.RespResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@RestController
@RequestMapping("/goods/spu")
@Api(tags = "spu相关接口")
public class SpuController {

    @Autowired
    private SpuService spuService;

    /***
     * 保存
     */
    @PostMapping(value = "/save")
    @ApiOperation("新增商品")
    public RespResult<Boolean> save(@RequestBody Product product){
        //保存
        spuService.saveProduct(product);
        return RespResult.ok(true);
    }

}


package com.ymw.mall.goods.service;

import com.ymw.mall.goods.entity.CategoryAttr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 属性分类表 服务类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
public interface CategoryAttrService extends IService<CategoryAttr> {

}

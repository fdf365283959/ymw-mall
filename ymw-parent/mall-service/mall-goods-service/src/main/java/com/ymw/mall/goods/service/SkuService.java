package com.ymw.mall.goods.service;

import com.ymw.mall.goods.entity.Sku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
public interface SkuService extends IService<Sku> {

}

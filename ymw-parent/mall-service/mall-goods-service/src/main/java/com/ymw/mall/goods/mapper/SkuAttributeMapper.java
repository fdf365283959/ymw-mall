package com.ymw.mall.goods.mapper;

import com.ymw.mall.goods.entity.SkuAttribute;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 属性表 Mapper 接口
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Repository
public interface SkuAttributeMapper extends BaseMapper<SkuAttribute> {

    /***
     * 根据分类ID查询属性集合
     * @param id
     * @return
     */
    @Select("SELECT * FROM sku_attribute WHERE id IN(SELECT attr_id FROM category_attr WHERE category_id=#{id})")
    List<SkuAttribute> queryByCategoryId(Integer id);

}

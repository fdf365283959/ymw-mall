package com.ymw.mall.goods.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 属性分类表 前端控制器
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Controller
@RequestMapping("/goods/categoryAttr")
public class CategoryAttrController {

}


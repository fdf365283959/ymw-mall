package com.ymw.mall.goods.controller;


import com.ymw.mall.goods.entity.SkuAttribute;
import com.ymw.mall.goods.service.SkuAttributeService;
import com.ymw.mall.util.RespResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 属性表 前端控制器
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@RestController
@RequestMapping("/goods/skuAttribute")
@Api(tags = "sku接口")
public class SkuAttributeController {

    @Autowired
    SkuAttributeService skuAttributeService;

    /***
     * 根据分类ID查询
     */
    @GetMapping(value = "/category/{id}")
    @ApiOperation("根据分类ID查询")
    public RespResult<List<SkuAttribute>> categoryAttributeList(@PathVariable(value = "id")Integer id){
        //根据分类ID查询属性参数
        List<SkuAttribute> skuAttributes = skuAttributeService.queryList(id);
        return RespResult.ok(skuAttributes);
    }

}


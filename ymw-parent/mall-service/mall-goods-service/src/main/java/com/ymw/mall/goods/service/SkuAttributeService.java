package com.ymw.mall.goods.service;

import com.ymw.mall.goods.entity.SkuAttribute;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 属性表 服务类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
public interface SkuAttributeService extends IService<SkuAttribute> {


    /**
     * 根据分类ID查询属性集合
     */
    List<SkuAttribute> queryList(Integer id);

}

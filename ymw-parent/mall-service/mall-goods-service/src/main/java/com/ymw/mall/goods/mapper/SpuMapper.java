package com.ymw.mall.goods.mapper;

import com.ymw.mall.goods.entity.Spu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@Repository
public interface SpuMapper extends BaseMapper<Spu> {

}

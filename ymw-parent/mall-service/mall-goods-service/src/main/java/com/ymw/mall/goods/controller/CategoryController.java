package com.ymw.mall.goods.controller;


import com.ymw.mall.goods.entity.Category;
import com.ymw.mall.goods.service.CategoryService;
import com.ymw.mall.util.RespResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 商品类目 前端控制器
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
@RestController
@RequestMapping("/goods/category")
@CrossOrigin
@Api(tags = "商品分类接口")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /****
     * 根据父ID查询子分类
     */
    @GetMapping(value = "/parent/{pid}")
    @ApiOperation("根据父ID查询子分类")
    public RespResult<List<Category>> list(@PathVariable(value = "pid") Integer pid) {
        List<Category> categories = categoryService.queryByParentId(pid);
        return RespResult.ok(categories);
    }

}


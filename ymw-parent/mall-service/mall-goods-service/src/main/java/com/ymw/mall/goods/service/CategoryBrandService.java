package com.ymw.mall.goods.service;

import com.ymw.mall.goods.entity.CategoryBrand;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分类品牌关系表 服务类
 * </p>
 *
 * @author dongfengfan
 * @since 2021-08-04
 */
public interface CategoryBrandService extends IService<CategoryBrand> {

}

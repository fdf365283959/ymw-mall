package com.ymw.mall.util;

import lombok.Data;

import java.io.Serializable;

/**
 * @author dongfengfan
 * @date 2021/7/6 下午5:15
 * 统一返回结果
 */
@Data
public class RespResult<T> implements Serializable {

    //响应数据结果集
    private T data;

    /**
     * 状态码
     * 20000 操作成功
     * 50000 操作失败
     */
    private Integer code;

    /***S
     * 响应信息
     */
    private String message;

    public RespResult() {
    }

    public RespResult(RespCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public RespResult(T data, RespCode resultCode) {
        this.data = data;
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public static <T> RespResult<T> ok() {
        return new RespResult<T>(null, RespCode.SUCCESS);
    }

    public static <T> RespResult<T> ok(T data) {
        return new RespResult<T>(data, RespCode.SUCCESS);
    }

    public static <T> RespResult<T> error(String message) {
        return secByError(RespCode.ERROR.getCode(), message);
    }

    //自定义异常
    public static <T> RespResult<T> secByError(Integer code, String message) {
        RespResult<T> err = new RespResult<T>();
        err.setCode(code);
        err.setMessage(message);
        return err;
    }

    public static <T> RespResult<T> error(RespCode resultCode) {
        return new <T>RespResult<T>(resultCode);
    }

}
